/*
 * Manik Khosla
 * Version 1.0.0  26 july,2018
 * Input Marks in Array
 *  
 *  Copyright (c) 2010-2018,InTimeTec Visionsoft Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of 
 * InTimeTec Visionsoft, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with InTimeTec.
 */


import java.util.*;
/**
 * 
 * @author manik.khosla
 *
 */
public class InputMarksInArray {
	/**
	 *Input Integer  Marks in array 
	 */
	public int[]  inputIntegerMarks(int[] array,int lengthOfArray){
		boolean success = false;                                                    //Made Boolean value
		Scanner inputMarks=new Scanner(System.in);	
		System.out.println("Please Enter Elements");
		for(int i=0;i<lengthOfArray;i++){
			try{
				array[i]=inputMarks.nextInt();
				success=true;
			}catch(InputMismatchException exception){
				System.out.println("You have Entered wrong Element in Array.It Can Only Store Integer Elements");
				success=true;
				break;
			}

		}
		if(success==true){
			System.out.println("Elements Have Been Entered in Array successfully");
			return array;
		}else{
			return null;
		}
	}

	/**
	 *Input Float Marks in array 
	 */
	public float[]  inputFloatMarks(float[] array,int lengthOfArray){
		boolean success=false;
		Scanner inputMarks=new Scanner(System.in);	
		System.out.println("Please Enter Elements");
		for(int i=0;i<lengthOfArray;i++){
			try{
				array[i]=inputMarks.nextFloat();
				success=true;
			}catch(InputMismatchException exception){
				System.out.println("You have Entered wrong Element in Array.It Can Only Store Float Elements");
				success=true;
				break;
			}
		}
		if(success==true){
			System.out.println("Elements Have Been Entered in Array successfully");
			return array;
		}else{
			return null;
		}
	}

	/**
	 * Ask User How many Elements user wants to enter
	 * @return
	 */
	public int enterLengthOfArray(){                                          //Changed Name of Function
		boolean success;
		int length = 0;
		Scanner scanlength=new Scanner(System.in);
		System.out.println("Please enter Number of Elements you want to enter in Array ");
		try{
			length=scanlength.nextInt();
			success=true;
		}catch(InputMismatchException exception){
			System.err.println("Please Enter an Integer Value");
			success=false;
		}
		if(success==true){
			return length;
		}else{
			return 0;
		}
	}

	/**
	 * Asking User whether he wishes to view elements entered in array or Not
	 */
	public String viewElementsChoice(){
		String viewElements="";
		Scanner viewElementsChoice=new Scanner(System.in);
		System.out.println("Do You wish To View Elements Entered In Array.Please Enter Yes/No");
		viewElements=viewElementsChoice.next();
		if(viewElements.equals("Yes") || viewElements.equals("No")){
			return viewElements;
		}else{
			System.err.println("You have entered an invalid choice");
			return null;
		}
	}

	/**
	 * Print Integer array
	 */
	public void displayIntegerArray(int[] array,int lengthOfArray){
		for(int i=0;i<lengthOfArray;i++){
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}

	/**
	 * Print Float array
	 */
	public void displayFloatArray(float[] array,int lengthOfArray){
		for(int i=0;i<lengthOfArray;i++){
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}

	/**
	 * Main Method declaration
	 * @param args
	 */
	public static void main(String[] args){
		int choice=0;                                             
		int lengthOfArray;
		String viewElementsInArray="";
		InputMarksInArray marks=new InputMarksInArray();

		Scanner scan=new Scanner(System.in);

		Dowhileloop:
			do{
				System.out.println("Enter your Choice");
				System.out.println("Press 1 to enter Integer Values");
				System.out.println("Press 2 to enter Float Values");
				choice=scan.nextInt();

				if(choice==1){
					lengthOfArray=marks.enterLengthOfArray();
					if(lengthOfArray==0){
						break Dowhileloop;
					}
					int [] arr=new int[lengthOfArray];
					int[] array=marks.inputIntegerMarks(arr,lengthOfArray);
					if(array==null){
						break ;
					}
					viewElementsInArray=marks.viewElementsChoice();
					if(viewElementsInArray==null){
						break Dowhileloop;
					}else if(viewElementsInArray.equals("Yes")){
						marks.displayIntegerArray(array, lengthOfArray);
					}
				}
				else if(choice==2){
					lengthOfArray=marks.enterLengthOfArray();
					if(lengthOfArray==0){
						break Dowhileloop;
					}
					float [] arr=new float[lengthOfArray];
					float[] array=marks.inputFloatMarks(arr,lengthOfArray);
					if(array==null){
						break;
					}
					viewElementsInArray=marks.viewElementsChoice();
					if(viewElementsInArray==null){
						break Dowhileloop;
					}else if(viewElementsInArray.equals("Yes")){
						marks.displayFloatArray(array, lengthOfArray);
					}
				}
				else{
					System.out.println("You have Entered an invalid choice");
					break Dowhileloop;
				}
			}while(choice!=0);

		System.err.println("Program Terminated");
	}
}
