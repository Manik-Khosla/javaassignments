package com.intimetec.employee;
import java.util.ArrayList;

public class SetandGetEmployees {
	private int id;
	private String name;
	private String technology;

	/* Constructor declaration */
	public SetandGetEmployees(int ID,String Name,String Technology){
		id=ID;
		name=Name;
		technology=Technology;
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getTechnology() {
		return technology;
	}



	public void setTechnology(String technology) {
		this.technology = technology;
	}



	/* View employee details function declaration */
	public static SetandGetEmployees ViewEmployeeDetails(int id,ArrayList<SetandGetEmployees> arrayList){
	   
		for(SetandGetEmployees emp:arrayList){
		
			if(emp.id==id){
			return emp ;
			}else if(emp==null){
			return null;
			}
		}
		return  null;
		
	}
	
	

}
