package com.intimetec.employee;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class EmployeeOperations {

	public static void main(String[] args) throws IOException{
		Scanner scan=new Scanner(System.in);
		ArrayList<SetandGetEmployees> empArray=new ArrayList<SetandGetEmployees>();
		int choice;                                              /* switch case choice */
		SetandGetEmployees existingEmpView=null;
		SetandGetEmployees existingEmpUpdate=null;
		SetandGetEmployees empDelete=null;
		FileOutputStream fileOutput=new FileOutputStream("C:\\Eclipse\\Employee Data.txt");
		FileInputStream fileInput=new FileInputStream("C:\\Eclipse\\Employee Data.txt");
		/**
		 * while creating new employee adding  details 
		 */
		int empId;                                                
		String empName;                                           
		String empTechnology;                                       
        
		do{
			System.out.println("---------------Employee Manement System---------------\n"
					+ "Please Enter your choice\n"
					+ "1. Insert new Employee\n"
					+ "2. View Existing Employee Details\n"
					+ "3. Update Existing Employee Details\n"
					+ "4. Delete Existing Employee\n"
					+ "5. View All Employee Details\n");

			choice=scan.nextInt();

			switch(choice){

			case 1:System.out.println("Enter employee id");
			empId=scan.nextInt();
			System.out.println("Enter employee Name");
			scan.nextLine();
			empName=scan.nextLine();
			System.out.println("Enter Technology employee will working on");
			empTechnology=scan.nextLine();

			/* Creating new employee */
			SetandGetEmployees newemp=new SetandGetEmployees(empId,empName,empTechnology);

			/*Adding new employee in array list */
			empArray.add(newemp);

			/* writing data to a file */
			try{
			String employeeData=empId+" "+empName+" "+empTechnology+"\r\n";
			fileOutput.write(employeeData.getBytes());
			System.out.println("Employee added Successfully with the following details");
			}catch(Exception e){
				System.out.println("Error occured while writing data to a file");
			}
			
	     	break;
  

			case 2:System.out.println("Enter employee id to view employee details");
			empId=scan.nextInt();
			try{
			existingEmpView=SetandGetEmployees.ViewEmployeeDetails(empId, empArray);
			if(existingEmpView==null){
			   throw new NullPointerException();}
			else if(existingEmpView.getId()==0){
			System.out.println("Employee Not Found with this Id");
			break;
			   }
			}catch(NullPointerException e){
				System.out.println("Data is empty.Please Create a new Employee first");
				break;
			}
			System.out.println("Employee ID->"+existingEmpView.getId()+"  "+"Employee Name->"+existingEmpView.getName()+"  "+"Employee Technology->"+existingEmpView.getTechnology());
			break;




			case 3:System.out.println("Enter employee id to Update employee details");
			empId=scan.nextInt();
			try{
			 existingEmpUpdate=SetandGetEmployees.ViewEmployeeDetails(empId, empArray);
			 if(existingEmpUpdate==null){
		    throw new NullPointerException();}
			
			}catch(NullPointerException e){
				System.out.println("ArrayList is empty.Please Create a new Employee first");
				break;
			} 
			System.out.println("Enter your choice");
			System.out.println("1.Update Name");
			System.out.println("2.Update Technology");
			int updateChoice=scan.nextInt();

			switch(updateChoice){

			case 1:System.out.println("Enter New Name");
			scan.nextLine();
			String newName=scan.nextLine();
			existingEmpUpdate.setName(newName);
			String newEmployeeData=existingEmpUpdate.getId()+" "+existingEmpUpdate.getName()+" "+existingEmpUpdate.getTechnology()+"\r\n";
			/* Updating in file */
			fileOutput.write(newEmployeeData.getBytes()); 
			
			System.out.println("Employee details Updated successfully");
			for(SetandGetEmployees emp:empArray){

				System.out.println("Employee ID->"+emp.getId()+"  "+"Employee Name->"+emp.getName()+"  "+"Employee Technology->"+emp.getTechnology());
			}
			break;

			case 2:System.out.println("Enter New Technology");
			scan.nextLine();
			String newTechnology=scan.nextLine();
			existingEmpUpdate.setTechnology(newTechnology);
			String newEmployeeData1="Updated->"+existingEmpUpdate.getId()+" "+existingEmpUpdate.getName()+" "+existingEmpUpdate.getTechnology()+"\r\n";
				try {
					fileOutput.write(newEmployeeData1.getBytes());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			System.out.println("Employee details Updated successfully");
			for(SetandGetEmployees emp:empArray){

				System.out.println("Employee ID->"+emp.getId()+"  "+"Employee Name->"+emp.getName()+"  "+"Employee Technology->"+emp.getTechnology());
			}
			break;
			}
			break;

			case 4:System.out.println("Enter employee id to Delete employee details");
			empId=scan.nextInt();
			try{
		     empDelete=SetandGetEmployees.ViewEmployeeDetails(empId, empArray);
		     if(empDelete==null){
		    	 throw new NullPointerException();}
			}catch(NullPointerException e){
			System.out.println("ArrayList is empty.Please Create a new Employee first");
			break;
			}
			empArray.remove(empDelete);
			System.out.println("Employee deleted successfully");
			for(SetandGetEmployees emp:empArray){

				System.out.println("Employee ID->"+emp.getId()+"  "+"Employee Name->"+emp.getName()+"  "+"Employee Technology->"+emp.getTechnology());
			}
			break;

			case 5:for(SetandGetEmployees emp:empArray){

				System.out.println("Employee ID->"+emp.getId()+"  "+"Employee Name->"+emp.getName()+"  "+"Employee Technology->"+emp.getTechnology());
			}
			break;

			default:System.out.println("You have entered an invalid choice");
			}
		}while(choice>=1);

	}

}
