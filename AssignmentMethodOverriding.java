
public class AssignmentMethodOverriding {
	
	public static void main(String[] args){
		
	/* Creating instances of Banks */
	Bank bank=new Bank();
	IciciBank icici=new IciciBank();
	SbiBank sbi=new SbiBank();
	AxisBank axis=new AxisBank();
	
	/* Printing ROI Of different banks */
	System.out.print("Bank Rate Of Interest= "+bank.getRateOfInterest()+"\n"
			+ "Icici Bank Rate Of Interest= "+icici.getRateOfInterest()+"\n"
			+ "Sbi Bank Rate Of Interest= "+sbi.getRateOfInterest()+"\n"
			+ "Axis Bank Rate Of Interest= "+axis.getRateOfInterest());
	
	}
} 
	
   /**
    * @author manik.khosla
    * Bank class declaration
    */
	class Bank{
	
		public int getRateOfInterest(){
		return 0;
		}
	}
	
	/**
	 * @author manik.khosla
	 * Icici Bank class Declaration
	 */
	class IciciBank extends Bank{
	
     /* Common for all Icici Bank Objects */
    static int  iciciBankRateOfInterest=7;    
    
    /* Method Overriding */
    public int getRateOfInterest(){
    return iciciBankRateOfInterest;	
    }
	}
	
	
	/**
	 * @author manik.khosla
	 * Sbi bank class declaration
	 */
	class SbiBank extends Bank{
	
	/* Common for all Sbi Bank Objects */
	static int sbiBankRateOfInterest=8;                                                          
    
	/* Method Overriding */
    public int getRateOfInterest(){
    return sbiBankRateOfInterest;	
    }
	}
	
	
	/**
	 * @author manik.khosla
	 * Axis Bank class declaration
	 */
	class AxisBank extends Bank{
	
    /* Common for all axis Bank Objects */
	static int axisBankRateOfInterest=9;    
	
	/* Method Overriding */
    public int getRateOfInterest(){
    return axisBankRateOfInterest;	
    }
		
	}


