/*
 * Manik Khosla
 * Version-1.0.0 25 July,2018
 * Assignment 1-Program to create a string of length �n�. The value of �n� is the sum of the user input
 * 
 * Copyright (c) 2010-2018,InTimeTec Visionsoft Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of 
 * InTimeTec Visionsoft, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with InTimeTec.
 */

import java.util.*;
/**
 * 
 * @author manik.khosla
 * Assignment 1 -Generating Random Strings
 *
 */
public class GenerateRandomString {

	int num1 = 0;                                                                   /* Declaring first number */
	int num2 = 0;                                                                   /* Declaring second number */
	int sum;                                                                        /* Taking sum of 2 numbers */
	String orignal="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_+={}[]<>?/\\";
	String outputString="";                                                         /* Output */

	/**
	 * Calculate sum
	 */
	public void caluclateSum(){
		sum=num1+num2; 
		System.out.println("Sum is "+sum);
	}

	/**
	 * Generating Random Values
	 */
	public void generateRandomValue(){
		for(int i=0;i<sum;i++){
			int randomValue=(int) (sum*((Math.round(Math.random()*100)+9)));//   use round function
			/** If Random Value is Greater Than String Index Then Bringing Value With In Index */
			if(randomValue>orignal.length()){
				randomValue=randomValue%orignal.length();                                              /* If Random Value<80 */
				outputString+=orignal.charAt(randomValue);
			}else{                                            
				outputString+=orignal.charAt(randomValue);
			}
		}
	}

	/**
	 * Printing Final Result
	 */
	public void displayOutput(){
		System.out.println("Random String Generated is");
		System.out.println(outputString);
		outputString="";
	}

	/**
	 * Main method
	 */
	public static void main(String[] args){
		Scanner scan=new Scanner(System.in);

		GenerateRandomString object=new GenerateRandomString();                      /* Creating an object */

		dowhileloop:
			do{                                                                      /* loop start */

				try{                                                                 /* try block  start*/
					System.out.println("Enter First Number");
					object.num1=scan.nextInt();                                      /* Input first number */
					System.out.println("Enter Second Number");
					object.num2=scan.nextInt();                                      /* Input Second NUmber */
				}                                                                    /* try block end */
				catch(InputMismatchException exception){                             /* catch block start */
					System.out.println("You have Entered a Character.Please Enter a Number");
					break dowhileloop;
				}                                                                    /* catch block end  */
				if(object.num1<=0 && object.num2<=0){
					System.out.println("You have Entered numbers less than or equal to 0.Please Enter  positive numbers ");
					break dowhileloop;
				}else{
					object.caluclateSum();                                           /* Invoking CalulateSum method */
					object.generateRandomValue();                                    /* Invoking generateRandomValue method */
					object.displayOutput();                                          /* Invoking displayOutput method */
				}                                                                    /* Else block termination */

			}while(object.num1!=0 || object.num2!=0);


		System.err.println("Program Terminated");
	}

}
